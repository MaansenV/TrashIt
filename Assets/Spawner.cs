using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereSpawnController : MonoBehaviour
{
    public float spawnRadius = 5f; // Der Radius der Sphere um den Spieler
    public float spawnInterval = 2f; // Zeit zwischen den Spawns in Sekunden
    public float startDelay = 5f; // Zeitverzögerung beim Starten der Coroutine nach Spielbeginn

    private void Start()
    {
        // Starte die Spawn-Routine nach der festgelegten Startverzögerung
        StartCoroutine(StartSpawning());
    }

    IEnumerator StartSpawning()
    {
        yield return new WaitForSeconds(startDelay);
        StartCoroutine(SpawnEnemies());
    }

    IEnumerator SpawnEnemies()
    {
        while (true) // Diese Schleife läuft unendlich
        {
            SpawnPrefabOnSphereEdge();

            // Warte das definierte Intervall, bevor der nächste Gegner gespawnt wird
            yield return new WaitForSeconds(spawnInterval);
        }
    }

    void SpawnPrefabOnSphereEdge()
    {
        // Zufälliger Punkt auf der Sphere-Oberfläche
        Vector3 randomPointOnSphere = Random.onUnitSphere;

        // Position für das Prefab berechnen
        Vector3 spawnPosition = GameManager.Instance.player.transform.position + randomPointOnSphere * spawnRadius;

        // Prefab spawnen
        ObjectPool.Instance.SpawnFromPool("Zombie", spawnPosition, Quaternion.identity);
    }

    // Zeige die Sphere im Editor mithilfe von Gizmos
    void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, spawnRadius);
    }
}


