using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    public Transform player; // Spielerreferenz
    public float moveSpeed = 3f;
    public float attackDistance = 2f;
    public float attackCooldown = 2f;
    public float dashDistance = 5f;
    public float dashCooldown = 4f;
    public float idleTime = 2f;
    
    public Animator anim;
    
    public bool canAttack = true;
    public bool canDash = true;
    private Transform spriteTransform; // Referenz auf das Sprite-Child-Objekt
    public double desiredMaxDistance;
    private static readonly int IsMoving = Animator.StringToHash("isMoving");

    void Start()
    {
        player = GameManager.Instance.player.transform;
        spriteTransform = transform.GetChild(0); // Annahme: Der Sprite ist das erste Kindobjekt
    }

    void Update()
    {
        // Berechne die Distanz zum Spieler
        float distanceToPlayer = Vector3.Distance(transform.position, player.position);

        // Wenn die Distanz kleiner als die Angriffsdistanz ist, greife an
        if (distanceToPlayer <= attackDistance && canAttack)
        {
            AttackPlayer();
        }
        else if (distanceToPlayer <= dashDistance && canDash)
        {
            DashToPlayer();
        }
        else
        {
            // Bewege zur Spielerposition
            MoveTowardsPlayer();
        }
    }

    void MoveTowardsPlayer()
    {
        // Berechne die Richtung und Distanz zum Spieler
        Vector3 directionToPlayer = player.position - transform.position;
        float distanceToPlayer = directionToPlayer.magnitude;

        // Bewege dich in Richtung des Spielers bis zur maximalen Distanz
        if (distanceToPlayer > desiredMaxDistance)
        {
            anim.SetBool(IsMoving,true);
            // Normalisiere die Richtung und bewege dich in dieser Richtung
            directionToPlayer.Normalize();
            transform.Translate(directionToPlayer * moveSpeed * Time.deltaTime);
        }
        else
        {
            anim.SetBool(IsMoving,false);
        }

        // Passe den Scale des Sprite-Child-Objekts an
        if (directionToPlayer.x > 0)
        {
            spriteTransform.localScale = new Vector3(1f, 1f, 1f);
        }
        else if (directionToPlayer.x < 0)
        {
            spriteTransform.localScale = new Vector3(-1f, 1f, 1f);
        }
    }


    void AttackPlayer()
    {
        // Führe den Angriff aus
        Debug.Log("Attack!");

        // Setze den Angriffscooldown
        StartCoroutine(AttackCooldown());
    }

    IEnumerator AttackCooldown()
    {
        canAttack = false;
        yield return new WaitForSeconds(attackCooldown);
        canAttack = true;
    }

    void DashToPlayer()
    {
        // Führe den Dash aus
        Debug.Log("Dash!");

        // Setze den Dash-Cooldown
        StartCoroutine(DashCooldown());
        
        // Setze die KI in den Idle-Status
        StartCoroutine(IdleState());
    }

    IEnumerator DashCooldown()
    {
        canDash = false;
        yield return new WaitForSeconds(dashCooldown);
        canDash = true;
    }

    IEnumerator IdleState()
    {
        yield return new WaitForSeconds(idleTime);

        // Hier könntest du zusätzliche Aktionen während der Idle-Zeit einfügen

        // Setze die KI zurück in den normalen Zustand
    }
}
