﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using Unity.VisualScripting;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class Gun : MonoBehaviour
{
    public Transform gunTransform; // Referenz zum Gun-Child-Objekt

    public List<ParticleSystem> gunList;
    public AudioSource gunSound;
    void Update()
    {
        // Waffe zur Maus rotieren
        
        RotateGunToMouse();
        SetGunScale();

        EmitParticles();
    }

    void RotateGunToMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition); // Wandelt Bildschirmkoordinaten in Weltkoordinaten um

        Vector2 direction = new Vector2(mousePosition.x - gunTransform.position.x, mousePosition.y - gunTransform.position.y);

        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        gunTransform.right = direction; // Setzt die rechte Achse des Gun-Transforms in die Richtung der Maus
    }

    void SetGunScale()
    {
        if (transform.localScale != Vector3.one)
        {
            gunTransform.localScale = new Vector3(-1, -1, 0);
        }
        else
        {
            gunTransform.localScale = Vector3.one;
        }
    }

    public void EmitParticles()
    {
        if (Input.GetMouseButtonDown(0))
        {
            gunSound.Play();
            gunList[0].Emit(1);
        }
        
    }
    
}