using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float moveSpeed = 5f;
    public Rigidbody2D rb;
    public Animator anim;
    Vector2 movement;
    private static readonly int IsMoving = Animator.StringToHash("isMoving");

    private void Awake()
    {
        anim = GetComponentInChildren<Animator>();
    }

    void Update()
    {   
        //Input
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        movement.Normalize(); // Damit der Spieler sich nicht schneller diagonal bewegt

        // Direction to look at
        RotateToMouse();
        UpdateAnimation();
    }

    void FixedUpdate()
    {
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }

    void RotateToMouse()
    {
        Vector3 mousePosition = Input.mousePosition;
        mousePosition = Camera.main.ScreenToWorldPoint(mousePosition); // Wandelt Bildschirmkoordinaten in Weltkoordinaten um

        Vector2 direction = new Vector2(mousePosition.x - transform.position.x, mousePosition.y - transform.position.y);
        
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        
        if (angle > 90f || angle < -90f) // Wenn die Maus hinter dem Spieler ist (links)
        {
            transform.localScale = new Vector3(-Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
        else // Wenn die Maus vor dem Spieler ist (rechts)
        {
            transform.localScale = new Vector3(Mathf.Abs(transform.localScale.x), transform.localScale.y, transform.localScale.z);
        }
    }
    
    void UpdateAnimation()
    {
        if (movement != Vector2.zero)
        {
            anim.SetBool(IsMoving, true);
        }
        else
        {
            anim.SetBool(IsMoving, false);
        }
    }
}


