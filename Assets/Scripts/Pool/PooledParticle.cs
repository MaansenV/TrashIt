﻿using System;
using System.Collections;
using UnityEngine;

public class PooledParticle : MonoBehaviour
{
    [SerializeField]
    private bool waitForChildren;

    private ParticleSystem particleSystem;
    
    [SerializeField] private String poolTag;

    private void Awake()
    {
        particleSystem = GetComponent<ParticleSystem>();
    }

    private void OnEnable()
    {
        StartCoroutine(CheckIfParticleHasFinished());
    }

    private IEnumerator CheckIfParticleHasFinished()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.5f);

            if (!particleSystem.IsAlive(waitForChildren))
            {
                ObjectPool.Instance.ReturnToPool(poolTag, gameObject);
                break;
            }
        }
    }
}