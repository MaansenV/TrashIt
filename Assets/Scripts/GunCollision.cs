using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GunCollision : MonoBehaviour
{
    public int damage = 1;
    private void OnParticleCollision(GameObject other)
    {
        if(other.CompareTag("AI"))
        {
            Debug.Log(other.gameObject.name);
            other.GetComponent<Health>().GetHit(damage);
        }
    }
}
