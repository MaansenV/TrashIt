using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Health : MonoBehaviour
{
    public int MaxHealth = 3;

    private int currentHealth;
    public bool isPlayer = false;
    public TextMeshProUGUI healtValue;
    
    public AudioSource zombieSoundHit;
    public AudioSource zombieSoundDeath;
    private void OnEnable()
    {
       
        currentHealth = MaxHealth;
        
        if (isPlayer)
        {
            healtValue.text = currentHealth.ToString();
        }
    }
    

    public void GetHit(int dmg)
    {
        if (currentHealth > 0)
        {
            currentHealth -= dmg;
            ObjectPool.Instance.SpawnFromPool("Blood", transform.position, Quaternion.identity);
            if (isPlayer)
            {
                healtValue.text = currentHealth.ToString();
            }
            zombieSoundHit.Play();
        }
        else
        {
            if (isPlayer)
            {
                healtValue.text = currentHealth.ToString();
            }
            //zombieSoundDeath.Play();
            Die();
            ObjectPool.Instance.SpawnFromPool("BloodSplatter", transform.position, Quaternion.identity);
        }
    }

    public void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        { 
            other.gameObject.GetComponent<Health>().GetHit(1);
        }
    }

    void Die()
    {
        if (gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
        else
        {
            ObjectPool.Instance.ReturnToPool("Zombie", gameObject);
        }
        
    }
    
}
